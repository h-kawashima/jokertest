﻿using UnityEngine;
using System.Collections;
using Novel;

public class OpeningScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		RectTransform objectRectTransform = gameObject.GetComponent<RectTransform> ();
         Debug.Log("width: " + objectRectTransform.rect.width + ", height: " + objectRectTransform.rect.height);
		 
#if (UNITY_IOS || UNITY_ANDROID)
		string videoPath = "pico.mp4";
		Handheld.PlayFullScreenMovie(videoPath, Color.black, FullScreenMovieControlMode.CancelOnInput);
#endif
		NovelSingleton.StatusManager.callJoker("wide/newgame","");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
