﻿using UnityEngine;
using System.Collections;

public class PinchImageScene : MonoBehaviour {
	PinchScalingManager psm;

	// Use this for initialization
	void Start () {
		psm = GameObject.Find("PinchScalingManager").GetComponent<PinchScalingManager>();

		string imgPath = Novel.StatusManager.variable.get("f.ImgName");
		psm.pinchImage.sprite = Resources.Load<Sprite> (imgPath);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
