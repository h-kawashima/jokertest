﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;	//これを忘れずに

namespace Novel{

	public class SliderComponent : AbstractComponent {

		public SliderComponent ()
		{
			//必須項目
			this.arrayVitalParam = new List<string> {
				"name","x","y","value","min","max","var","target"
			};

			this.originalParam = new Dictionary<string,string> () {
				{"name",""},
				{"x",""},
				{"y",""},
				{"value",""},
				{"min",""},
				{"max",""},
				{"var", ""},
				{"target",""}
			};
		}


		public override void start ()
		{
			string _name = this.param["name"];
			string _x = this.param["x"];
			string _y = this.param["y"];
			string _value = this.param["value"];
			string _min = this.param["min"];
			string _max = this.param["max"];
			string _var = this.param["var"];
			string _target = this.param["target"];

			GameObject canvas = GameObject.Find ("Canvas");

			// InputFieldのプレハブを取得
			GameObject prefab = (GameObject)Resources.Load ("Prefabs/JokerSlider");

			//InputFieldを生成、生成したオブジェクトを変数に代入
			Slider sliderObj = (GameObject.Instantiate(prefab) as GameObject).GetComponent<Slider>();
		
			// Objecet name
			sliderObj.gameObject.name = _name;

			// position
			Vector3 pos = sliderObj.transform.position;
			pos.x = Int32.Parse(_x);
			pos.y = Int32.Parse(_y);
			sliderObj.transform.position = pos;
			
			sliderObj.wholeNumbers = true;

			// propertys
			sliderObj.minValue = Int32.Parse(_min);
			sliderObj.maxValue = Int32.Parse(_max);
			sliderObj.value = Int32.Parse(_value);

			// update callback
			sliderObj.onValueChanged.AddListener((value)=> {
				//変数に結果を格納
				StatusManager.variable.set (_var, ((int)value).ToString());

				// コールバックイベントへ
				string JumpStr = "[jump target=" + _target + "]";
				this.gameManager.startTag(JumpStr);
			});

			//Canvasの子要素として登録する 
			sliderObj.transform.SetParent (canvas.transform, false);

			this.gameManager.nextOrder ();

		}
	}

	public class Slider_removeComponent:AbstractComponent
	{

		public Slider_removeComponent ()
		{

			//必須項目
			this.arrayVitalParam = new List<string> {
				"name",
			};

			this.originalParam = new Dictionary<string,string> () {
				{"name",""},
			};

		}

		public override void start ()
		{
			string _name = this.param["name"];
			GameObject IF = GameObject.Find (_name);
			GameObject.Destroy (IF);
			
			this.gameManager.nextOrder ();
		}
	}
}