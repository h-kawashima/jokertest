﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;	//これを忘れずに

namespace Novel{

	public class InputComponent : AbstractComponent {

		public InputComponent ()
		{

		}

		public override void start ()
		{
			GameObject canvas = GameObject.Find ("Canvas");

			// InputFieldのプレハブを取得
			GameObject prefab = (GameObject)Resources.Load ("Prefabs/InputField");

			//InputFieldを生成、生成したオブジェクトを変数に代入
			GameObject _inputfield =	GameObject.Instantiate(prefab); 
			_inputfield.gameObject.name = "InputField";

			//Canvasの子要素として登録する 
			_inputfield.transform.SetParent (canvas.transform, false);

			this.gameManager.nextOrder ();

		}
	}

	public class Input_setComponent:AbstractComponent
	{

		public Input_setComponent ()
		{

			//必須項目
			this.arrayVitalParam = new List<string> {
				"flag","target"
			};

			this.originalParam = new Dictionary<string,string> () {
				{"flag",""},
				{"target",""},
			};

		}


		public override void start ()
		{
			GameObject _input = GameObject.Find ("inputText");
			string str = _input.GetComponent<Text>().text;

			string var_name = this.param ["flag"];

			Debug.Log("InputText:" + str);

			if (str.Length > 0) {

				//変数に結果を格納
				StatusManager.variable.set (var_name, str);

				string JumpStr = "[jump target=" + this.param ["target"] + "]";
				
				GameObject IF = GameObject.Find ("InputField");
				GameObject.Destroy (IF);  //そんなによく使うもんじゃないだろうからデストロイしちゃいます
			
				//targetにジャンプ
				this.gameManager.startTag(JumpStr);

				//次のシナリオに進む処理
				//this.gameManager.nextOrder ();

			}


		}
	}
}