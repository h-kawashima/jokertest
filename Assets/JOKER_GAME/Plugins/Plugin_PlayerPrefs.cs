﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Novel{

	//
	//　設定データー読み込み用プラグイン
	//
	public class Get_ppComponent:AbstractComponent
	{
		public Get_ppComponent ()
		{
			
			//必須項目
			this.arrayVitalParam = new List<string> {
				"var","key",
			};
			//デフォルトではppという変数に格納する
			this.originalParam = new Dictionary<string,string> () {
				{"var","pp"},
				{"key","pp"},
				{"def",""},
			};
			
		}
		
		
		public override void start ()
		{

			//変数名を初期化
			string var_name = this.param ["var"];

			//キーを初期化
			string key = this.param ["key"];

			//一旦リザルトにデフォルトパラメータを入れる
			string arg_result = this.param["def"];

			//キーが存在しているかをチェック
			if (PlayerPrefs.HasKey (key)) {
				//キーが存在していたらキーの内容でデーターを上書きする
				arg_result = PlayerPrefs.GetString(key);
			}
			//変数に結果を格納
			StatusManager.variable.set (var_name, arg_result);
			
			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
			
		}
		
	}

	//
	//　設定データー書き込み用プラグイン
	//
	public class Set_ppComponent:AbstractComponent
	{
		public Set_ppComponent ()
		{
			
			//必須項目
			this.arrayVitalParam = new List<string> {
				"key","arg"
			};
			//デフォルトではppという変数に格納する
			this.originalParam = new Dictionary<string,string> () {
				{"key","pp"},
				{"arg",""},
			};
			
		}
		
		
		public override void start ()
		{
			
			//キーを初期化
			string key = this.param ["key"];

			//具を初期化
			string arg = this.param ["arg"];

			//PlayerPrefsに具を格納
			PlayerPrefs.SetString(key,arg);
			//DATAを永続化
			PlayerPrefs.Save ();
			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
			
		}
		
	}

	//
	//　設定を削除するプラグイン
	//
	public class Del_ppComponent:AbstractComponent
	{
		public Del_ppComponent ()
		{
			
			//必須項目
			this.arrayVitalParam = new List<string> {
				"key"
			};
			//デフォルトではppという変数に格納する
			this.originalParam = new Dictionary<string,string> () {
				{"key","pp"}
			};
			
		}
		
		
		public override void start ()
		{
			
			//キーを初期化
			string key = this.param ["key"];

			//PlayerPrefsに具を格納
			PlayerPrefs.DeleteKey(key);
			//DATAを永続化
			PlayerPrefs.Save ();

			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
			
		}
		
	}

	//
	//　全設定を削除するプラグイン
	//
	public class Alldel_ppComponent:AbstractComponent
	{
		public Alldel_ppComponent ()
		{
			
			//必須項目
			this.arrayVitalParam = new List<string> {
			};
			//変数なし
			this.originalParam = new Dictionary<string,string> () {
			};
			
		}
		
		
		public override void start ()
		{
			//PlayerPrefsに具を格納
			PlayerPrefs.DeleteAll ();
			//DATAを永続化
			PlayerPrefs.Save ();
			
			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
			
		}
		
	}

	//
	// 指定キーの指定インデクスのビットを操作する
	//
	public class Seteventflag_ppComponent:AbstractComponent	{
		public Seteventflag_ppComponent() {
			//必須項目
			this.arrayVitalParam = new List<string> {
				"key","index","flag"
			};
			this.originalParam = new Dictionary<string,string> () {
				{"key",""},
				{"index",""},
				{"flag",""},
			};
		}

		public override void start() {
			string key = this.param ["key"];
			int index = Int32.Parse(this.param["index"]);
			string flag_value = this.param["flag"];
			string key_dat = "null";
			int key_dat_int;

			//キーが存在しているかをチェック
			if (PlayerPrefs.HasKey (key)) {
				//キーが存在していたらキーの内容でデーターを上書きする
				key_dat = PlayerPrefs.GetString(key);
			}

			if (key_dat.Equals("null")) {
				key_dat_int = 0;
			} else {
				key_dat_int = Int32.Parse(key_dat);
			}

			if (flag_value.Equals("true")) {
				key_dat_int |= (1 << index);
			} else {
				key_dat_int &= ~(1 << index);
			}

			//PlayerPrefsに具を格納
			PlayerPrefs.SetString(key,key_dat_int.ToString());
			//DATAを永続化
			PlayerPrefs.Save ();
			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
		}
	}

	//
	// 指定キーの指定インデクスのビットが立っているか確認
	//
	public class Geteventflag_ppComponent:AbstractComponent	{
		public Geteventflag_ppComponent() {
			//必須項目
			this.arrayVitalParam = new List<string> {
				"var","key","index"
			};

			this.originalParam = new Dictionary<string,string> () {
				{"var",""},
				{"key",""},
				{"index",""},
			};
		}

		public override void start() {
			string var_name = this.param["var"];
			string key = this.param ["key"];
			int index = Int32.Parse(this.param["index"]);
			string key_dat = "null";
			int key_dat_int;
			string result = "false";

			//キーが存在しているかをチェック
			if (PlayerPrefs.HasKey (key)) {
				//キーが存在していたらキーの内容でデーターを上書きする
				key_dat = PlayerPrefs.GetString(key);
			}

			if (!key_dat.Equals("null")) {
				key_dat_int = Int32.Parse(key_dat);

				if ((key_dat_int & (1 << index)) != 0) {
					result = "true";
				}
			}
			
			//変数に結果を格納
			StatusManager.variable.set (var_name, result);

			//次のシナリオに進む処理
			this.gameManager.nextOrder ();
		}
	}
}