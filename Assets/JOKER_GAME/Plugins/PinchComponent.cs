﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;	//これを忘れずに

namespace Novel{

	public class Pinch_setComponent : Image_newComponent {

		public Pinch_setComponent ():base()
		{
			//画像のルートパスが異なってくる
			base.imagePath = GameSetting.PATH_IMAGE;
		}

		public override void start () {
			this.param ["imagePath"] = GameSetting.PATH_IMAGE;

			string filename = base.imagePath + param ["storage"];
			
			StatusManager.variable.set("f.ImgName", filename);

			// ピンチ操作が可能なシーンへ飛ぶ
			Application.LoadLevel("PinchImageScene");
		}
	}

	public class Pinch_removeComponent:AbstractComponent
	{

		public Pinch_removeComponent (){}

		public override void start ()
		{
			GameObject IF = GameObject.Find ("PinchObj");
			GameObject.Destroy (IF);
			
			this.gameManager.nextOrder ();
		}
	}
}